{
	util.reset()
}

start = (
	query ("&" expand)? ("&" query)? /
	expand ("&" query)?
) { return util.complete() }

name = a:[a-zA-Z0-9]+ { return a.join("") }
any = a:[a-zA-Z0-9]+ { return a.join("") }
number = a:[0-9]+ { return a.join("") }
order_type = "asc" / "desc"

query = exp (("&" exp)+)?

exp = (select / skip / top / orderby)

select = "$select=" select_list
select_list = e:name ("," select_list)? { util.setSelect(e) }

skip = "$skip=" a:number { util.setSkip(a) }

top = "$top=" a:number { util.setTop(a) }

orderby = "$orderby=" orderby_list
orderby_list = n:name " " t:order_type ("," orderby_list)? { util.setOrder(n, t) }

expand = "$expand=" identifierPathList
identifierPathList = res:identifierPath ("," identifierPathList)? { util.setExpand(res) }
identifierPath = l:(n:name "/"? { return n })+ { return l.join('/') }