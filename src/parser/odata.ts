export interface OData {
  select?: string[];
  filter?: ODataFilter;
  orderby?: ODataOrder[];
  skip?: number;
  top?: number;
  expand?: string[];
}

export enum ODataOrderType {
	ASC = 'asc',
	DESC = 'desc',
}

export interface ODataOrder {
	field: string,
	type: ODataOrderType
}

export interface ODataFilter {
  type: ODataFilterType;
  left: ODataFilterTypeLiteral | ODataFilterTypeProperty | ODataFilter;
  right: ODataFilterTypeLiteral | ODataFilterTypeProperty | ODataFilter;
}

export interface ODataFilterTypeLiteral {
  type: ODataFilterType.LITERAL;
  value: string;
}

export interface ODataFilterTypeProperty {
  type: ODataFilterType.PROPERTY;
  name: string;
}

export enum ODataFilterType {
  LITERAL = 'literal',
  PROPERTY = 'property',

  EQ = 'eq',
  NOT_EQ = 'ne',
  GREATER = 'gt',
  GR_EQ = 'ge',
  LESS = 'lt',
  LE_EQ = 'le',
  HAS = 'has',
  IS = 'in',

  AND = 'and',
  OR = 'or',
}
