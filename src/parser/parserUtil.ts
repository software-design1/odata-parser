import {
  OData,
  ODataFilter,
  ODataFilterType,
  ODataFilterTypeLiteral,
  ODataFilterTypeProperty,
  ODataOrderType,
} from "./odata";

// parser logic

let odata: OData = {};

export const reset = () => {
  odata = {};
};

export const complete = () => {
  return odata;
};

export const setSelect = (value: string) => {
	odata.select = [value, ...(odata.select ?? [])];
};

export const setSkip = (value: string) => {
  odata.skip = Number(value);
};

export const setTop = (value: string) => {
  odata.top = Number(value);
};

export const setOrder = (name: string, type: string) => {

  const typeEnum: ODataOrderType =
    type === "asc" ? ODataOrderType.ASC : ODataOrderType.DESC;

  odata.orderby = [
    {
      field: name,
      type: typeEnum,
    },
    ...(odata.orderby ?? []),
  ];
};

export const setExpand = (resource: string) => {
	odata.expand = [resource, ...(odata.expand ?? [])]
};
